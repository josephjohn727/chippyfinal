package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class Enemy {

    private int xPosition;
    private int yPosition;
    private Bitmap image;
    private Bitmap obstacle;

    Context context;
    private int lineXPosition = xPosition;
    private int lineYPosition = yPosition;

    private int environmentX = 200;
    private int environmentY = 200;

    private List<Bitmap> environmentRects;

    private Rect hitbox;


    private Rect enemyHitbox;


    public Enemy(Context context,int xPosition, int yPosition) {
        this.context = context;
        this.xPosition = xPosition ;
        this.yPosition = yPosition;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.fire1);
        this.obstacle = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy);
        environmentRects = new ArrayList<>();

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );


        this.enemyHitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.obstacle.getWidth(),
                this.yPosition + this.obstacle.getHeight()
        );
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public int getLineXPosition() {
        return lineXPosition;
    }

    public void setLineXPosition(int lineXPosition) {
        this.lineXPosition = lineXPosition;
    }

    public int getLineYPosition() {
        return lineYPosition;
    }

    public void setLineYPosition(int lineYPosition) {
        this.lineYPosition = lineYPosition;
    }

    public int getEnvironmentX() {
        return environmentX;
    }

    public void setEnvironmentX(int environmentX) {
        this.environmentX = environmentX;
    }

    public int getEnvironmentY() {
        return environmentY;
    }

    public void setEnvironmentY(int environmentY) {
        this.environmentY = environmentY;
    }

    public List<Bitmap> getEnvironmentRects() {
        return environmentRects;
    }

    public Rect getEnemyHitbox() {
        return enemyHitbox;
    }

    public void setEnemyHitbox(Rect enemyHitbox) {
        this.enemyHitbox = enemyHitbox;
    }


    public void setEnvironmentRects(List<Bitmap> environmentRects) {
        this.environmentRects = environmentRects;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    //Environment Creation
    //Environment Creation
    public List<Bitmap> createEnvironment(){
        //List<Rect> rects = new ArrayList<>();
        List<Bitmap> rects = new ArrayList<>();

        for (int i = 0;i < 3;i++){

            rects.add(obstacle);
            environmentRects.addAll(rects);
        }
        return rects;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }

    public void updateObstacleHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.obstacle.getWidth();
        this.hitbox.bottom = this.yPosition + this.obstacle.getHeight();
    }

}
