package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class RestartGame {

    private int xPosition;
    private int yPosition;
    Context context;
    private Bitmap image;

    public RestartGame(Context context, int xPosition, int yPosition) {
        this.context = context;
        this.xPosition = xPosition;
        this.yPosition = yPosition;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.arrows);
    }


    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }


}
