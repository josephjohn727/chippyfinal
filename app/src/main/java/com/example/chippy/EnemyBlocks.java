package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnemyBlocks {
    public int xCoord;
    public int yCoord;
    Context context;

    private List<Enemy> enemyList;
    private List<Rect> surroundingChips;
    private List<Rect> coreChips;
    private Map<Integer, List<Enemy>> enemyMap;
    private Map<Integer, List<Enemy>> enemyMapCopy;
    private List<Enemy> enemyListCopy;
    private int radius = 200;

    private boolean isFirstTime;

    private Bitmap image;

    public EnemyBlocks(Context context, int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.context = context;

        //this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.yellow_rect);

        surroundingChips = new ArrayList<>();
        coreChips = new ArrayList<>();

        enemyMapCopy = new HashMap<>();
        enemyListCopy = new ArrayList<>();


        createHitboxeOne();
        createHitboxesTwo();
        createThirdLevelHitboxes();
        createBottomLevelHitboxes();
        //createTentacleHitboxes();
        createCoreHitboxes();
    }

    public int getxCoord() {
        return xCoord;
    }

    public void setxCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    public int getyCoord() {
        return yCoord;
    }

    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    public List<Enemy> getEnemyList() {
        return enemyList;
    }

    public void setEnemyList(List<Enemy> enemyList) {
        this.enemyList = enemyList;
    }

    public Map<Integer, List<Enemy>> getEnemyMap() {
        return enemyMap;
    }

    public void setEnemyMap(Map<Integer, List<Enemy>> enemyMap) {
        this.enemyMap = enemyMap;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public List<Rect> getSurroundingChips() {
        return surroundingChips;
    }

    public void setSurroundingChips(List<Rect> surroundingChips) {
        this.surroundingChips = surroundingChips;
    }

    public List<Rect> getCoreChips() {
        return coreChips;
    }

    public void setCoreChips(List<Rect> coreChips) {
        this.coreChips = coreChips;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(boolean firstTime) {
        isFirstTime = firstTime;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Map<Integer, List<Enemy>> getEnemyMapCopy() {
        return enemyMapCopy;
    }

    public void setEnemyMapCopy(Map<Integer, List<Enemy>> enemyMapCopy) {
        this.enemyMapCopy = enemyMapCopy;
    }

    public void createHitboxeOne() {
        List<Rect> rects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Rect rect = new Rect(xCoord + 100 + 100 * i, yCoord - 200, xCoord +100 + 100 * i + 100, yCoord - 100);
            rects.add(rect);
            surroundingChips.addAll(rects);
        }
    }

    public void createHitboxesTwo() {
        List<Rect> rects = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Rect rect = new Rect(xCoord -30 + 360 * i, yCoord - 80, xCoord -30 + 360 * i + 100, yCoord + 20);
            rects.add(rect);
            surroundingChips.addAll(rects);
        }
    }

    public void createThirdLevelHitboxes() {
        List<Rect> rects = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Rect rect = new Rect(xCoord - 30 + 360 * i, yCoord + 20, xCoord - 30 + 360 * i + 100, yCoord + 120);
            rects.add(rect);
            surroundingChips.addAll(rects);
        }
    }

    public void createBottomLevelHitboxes() {
        List<Rect> rects = new ArrayList<>();
        Log.d("rect", "createBottomLevelHitboxes: ");
        for (int i = 0; i < 6; i++) {
            Rect rect = new Rect(xCoord - 200 + 100 * i, yCoord + 160, xCoord - 200 + 100 * i + 100, yCoord + 260);
            Log.d("inside loop", "createBottomLevelHitboxes: ");
            rects.add(rect);
            surroundingChips.addAll(rects);
        }
    }

    public void createCoreHitboxes() {
        List<Rect> rects = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            Rect rect = new Rect(xCoord + 100 * i, yCoord - 80, xCoord + 100 * i + 100, yCoord + 20);
            rects.add(rect);

            for (int j = 1; j <= 3; j++) {
                Rect rect1 = new Rect(xCoord + 100 * i, yCoord + 20, xCoord + 100 * i + 100, yCoord + 120);
                rects.add(rect1);
            }
        }
        coreChips.addAll(rects);
    }


    public void createEnemyBulletPattern(int xPosition, int yPosition,int key) {
        enemyList = new ArrayList<>();
        //enemyMap = new HashMap<>();

        int halfRadius = (radius / 2);

        createEnemy2(xPosition, yPosition, radius - halfRadius);
        createEnemy(xPosition, yPosition, radius);
        enemyListCopy.addAll(enemyList);
        //enemyMap.put(key,enemyList);
        //enemyMapCopy.put(key,enemyListCopy);
    }

    private void createEnemy(int xPosition, int yPosition, int radius) {
        int minX = xPosition - radius;
        int maxX = xPosition + radius;
        int minY = yPosition - radius;
        int maxY = yPosition + radius;
        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <= maxY; j++) {
                if ((i == minX || i == maxX) && (j == maxY || j == minY)) {
                    Enemy enemy = new Enemy(context, i, j);
                    //enemy.updateHitbox();
                    enemyList.add(enemy);
                }
            }
        }

    }

    private void createEnemy2(int xPosition, int yPosition, int radius) {
        int minX = xPosition - radius;
        int maxX = xPosition + radius;
        int minY = yPosition - radius;
        int maxY = yPosition + radius;
        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <= maxY; j++) {
                if (((i == minX || i == maxX) && (j == yPosition)) ||
                        ((i == xPosition) && (j == minY || j == maxY))) {
                    Enemy enemy = new Enemy(context, i, j);
                    //enemy.updateHitbox();
                    enemyList.add(enemy);
                }
            }
        }

    }
}